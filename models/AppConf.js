var mongoose = require('mongoose')

var AppsSchema = new mongoose.Schema({
  appl: String,
  v: Number,
  comment: String,
  conf: String,
  updated_date: { type: Date, default: Date.now }
})

var infotable = {
  appl: { edit: false, disabled: false, type: 'string', sortable: true, search: true },
  v: { edit: false, disabled: true, type: 'number', sortable: true, search: true },
  comment: { edit: true, disabled: false, type: 'string', sortable: false },
  conf: { edit: true, disabled: false, type: 'string', language: 'xml' },
  updated_date: { edit: false, disabled: true, type: 'string', sortable: true }
}

module.exports = {
  model: mongoose.model('apps_config', AppsSchema),
  infotable: infotable
}
