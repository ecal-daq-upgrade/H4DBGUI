var mongoose = require('mongoose')

var TBSchema = new mongoose.Schema({
  tag: String,
  v: Number,
  open: Boolean,
  comment: String,
  updated_date: { type: Date, default: Date.now }
})

var infotable = {
  tag: { edit: false, disabled: false, type: 'string', sortable: true },
  v: { edit: false, disabled: true, type: 'number', sortable: true },
  open: { edit: true, disabled: false, type: 'string', sortable: true },
  comment: { edit: true, disabled: false, type: 'string', sortable: true },
  updated_date: { edit: false, disabled: true, type: 'string', sortable: true }
}

module.exports = {
  model: mongoose.model('tb_camp', TBSchema),
  infotable: infotable
}
