var mongoose = require('mongoose')

var RunKeySchema = new mongoose.Schema({
  tag: String,
  v: Number,
  comment: String,
  applications: Object,
  updated_date: { type: Date, default: Date.now }
})

var infotable = {
  tag: {edit: false, disabled: false, type: 'string', sortable: true, search: true},
  v: {edit: false, disabled: true, type: 'number', sortable: true, search: true},
  comment: {edit: true, disabled: false, type: 'string', sortable: false},
  applications: { edit: true, disabled: false, type: 'object', language: 'json' },
  updated_date: { edit: false, disabled: true, type: 'string', sortable: true}
}

module.exports = {
  model: mongoose.model('run_key', RunKeySchema),
  infotable: infotable
}
