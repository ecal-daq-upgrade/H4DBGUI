var mongoose = require('mongoose')

var TableSchema = new mongoose.Schema({
  tag: String,
  v: Number,
  pos: Object,
  updated_date: { type: Date, default: Date.now }
})

var infotable = {
  tag: { edit: false, disabled: false, type: 'string', sortable: true },
  v: { edit: false, disabled: true, type: 'number', sortable: true },
  pos: { edit: true, disabled: false, type: 'object', language: 'json'},
  updated_date: { edit: false, disabled: true, type: 'string', sortable: true }
}

module.exports = {
  model: mongoose.model('table_pos', TableSchema),
  infotable: infotable
}
