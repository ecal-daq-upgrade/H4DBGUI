var mongoose = require('mongoose')

var SConfigSchema = new mongoose.Schema({
  tag: String,
  v: Number,
  comment: String,
  machines: Object,
  updated_date: { type: Date, default: Date.now }
})

var infotable = {
  tag: { edit: false, disabled: false, type: 'string', sortable: true },
  v: { edit: false, disabled: true, type: 'number', sortable: true },
  comment: { edit: true, disabled: false, type: 'string', sortable: false },
  machines: { edit: true, disabled: false, type: 'object', language: 'json' },
  updated_date: { edit: false, disabled: true, type: 'string', sortable: true }
}

module.exports = {
  model: mongoose.model('services_config', SConfigSchema),
  infotable: infotable
}
