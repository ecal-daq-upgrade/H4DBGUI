var mongoose = require('mongoose')

var RunInfoSchema = new mongoose.Schema({
  _id: Number,
  runType: String,
  scTag: String,
  scVer: Number,
  rkTag: String,
  rkVer: Number,
  activeDRs: Object,
  activeInputs: Object,
  startTime: Number,
  stopTime: Number,
  eventsPerSpill: Number,
  beamEnergy: Number,
  beamType: String,
  tablePosTag: String,
  tablePos: String,
  tableX: Number,
  tableY: Number,
  evinrun: Number,
  failed: Boolean,
  tbCamp: String

})

var infotable = {
  _id: { edit: false, disabled: true, type: 'number', sortable: true, search: true },
  tbCamp: { edit: false, disabled: true, type: 'string', search: true, sortable: true },
  runType: { edit: false, disabled: true, type: 'string' },
  scTag: { edit: false, disabled: true, type: 'string' },
  scVer: { edit: false, disabled: true, type: 'number' },
  rkTag: { edit: false, disabled: true, type: 'string' },
  rkVer: { edit: false, disabled: true, type: 'number' },
  activeDRs: { edit: false, disabled: true, type: 'object' },
  activeInputs: { edit: false, disabled: true, type: 'object' },
  startTime: { edit: false, disabled: true, type: 'string', sortable: true, search: true, date: true },
  stopTime: { edit: false, disabled: true, type: 'string', sortable: true, search: true, date: true },
  eventsPerSpill: { edit: false, disabled: true, type: 'number' },
  beamEnergy: { edit: false, disabled: true, type: 'number', sortable: true, search: true },
  beamType: { edit: false, disabled: true, type: 'string' },
  tablePosTag: { edit: false, disabled: true, type: 'string' },
  tablePos: { edit: false, disabled: true, type: 'string', sortable: true, search: true },
  tableX: { edit: false, disabled: true, type: 'number' },
  tableY: { edit: false, disabled: true, type: 'number' },
  evinrun: { edit: false, disabled: true, type: 'number', sortable: true, search: true },
  failed: { edit: false, disabled: true, type: 'string' }
}

module.exports = {
  model: mongoose.model('runs_info', RunInfoSchema),
  infotable: infotable
}
