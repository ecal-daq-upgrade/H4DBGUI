var mongoose = require('mongoose')

var PSConfigSchema = new mongoose.Schema({
    tag: String,
    v: Number,
    V1: Number,
    V2: Number,
    V3: Number,
    V4: Number,
    I1: Number,
    I2: Number,
    I3: Number,
    I4: Number,
    comment: String,
    updated_date: { type: Date, default: Date.now }
})

var infotable = {
    tag: {edit: false, disabled: false, type: 'string', sortable: true},
    v: {edit: false, disabled: false, type: 'number', sortable: true},
    V1: {edit: true, disabled: false, type: 'number'},
    V2: {edit: true, disabled: false, type: 'number'},
    V3: {edit: true, disabled: false, type: 'number'},
    V4: {edit: true, disabled: false, type: 'number'},
    I1: {edit: true, disabled: false, type: 'number'},
    I2: {edit: true, disabled: false, type: 'number'},
    I3: {edit: true, disabled: false, type: 'number'},
    I4: {edit: true, disabled: false, type: 'number'},
    comment: {edit: true, disabled: false, type: 'string', sortable: false},
    updated_date: { edit: false, disabled: true, type: 'string', sortable: true}
}

module.exports = {
  model: mongoose.model('power_supply_config', PSConfigSchema),
  infotable: infotable
}
