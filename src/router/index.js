import Vue from 'vue'
import Router from 'vue-router'

import ListTable from '@/components/ListTable'
import ShowRow from '@/components/ShowRow'
import CreateRow from '@/components/CreateRow'
import EditRow from '@/components/EditRow'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/list/:table',
      name: 'ListTable',
      component: ListTable
    },
    {
      path: '/show/:table/:id',
      name: 'ShowRow',
      component: ShowRow
    },
    {
      path: '/add/:table/:infotable',
      name: 'CreateRow',
      component: CreateRow
    },
    {
      path: '/edit/:table/:id',
      name: 'EditRow',
      component: EditRow
    }
  ]
})
