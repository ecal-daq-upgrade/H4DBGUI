var configs = require('../config/config.js')
var _ = require('lodash')
var winston = require('winston')

// Default configs for a logger.
// They can be overwritten by configuration
var defaults = function(name) {
  // Merging defauls with defaults from configs.
  return _.merge({
    console: {
      level: 'verbose',
      colorize: true,
      label: name,
      timestamp: true,
      stringify: true,
      prettyPrint: true
    },
    file: {
      level: 'warn',
      filename: './logs/' + name + '_log.txt',
      label: name,
      timestamp: true
    }
  }, configs.defaults)
}

// Loading the personalized loggers from the configs.loggers data.
// The defaults are merged with the user configuration.
_.forOwn(configs.loggers, (conf, name) => {
  // Adding a logger for element in logger config.
  winston.loggers.add(name,
    _.merge(defaults(name), conf)
  )
})

// The modules can ask for a logger. If the logger has not been configured
// by the user, It will be created with the default configuration.
module.exports = function(name) {
  if (!_.has(configs.loggers, name)) {
    // Adding logger bades on defaults
    winston.loggers.add(name, defaults(name))
  }
  return winston.loggers.get(name)
}