var express = require('express')
var router = express.Router()
var AppConf = require('../models/AppConf.js')
var _ = require('lodash')

router.get('/', function (req, res, next) {
  res.json({ infotable: AppConf.infotable })
})

router.post('/getRows', function (req, res, next) {
  AppConf.model.aggregate([
    { '$sort': { 'v': -1 } },
    {
      '$group': {
        '_id': '$appl',
        'appl': { '$first': '$appl' },
        'v': { '$first': '$v' },
        'comment': { '$first': '$comment' },
        'updated_date': { '$first': '$updated_date' },
        'trueid': { '$first': '$_id' }
      }
    },
    { '$sort': { 'appl': 1 } }
  ], function (err, rows) {
    if (err) return next(err)
    _.forEach(rows, (val) => {
      val._id = val.trueid
      delete val.trueid
    })
    let totalrows = rows.length
    var tableparams = {
      draw: req.body.draw,
      recordsTotal: totalrows,
      recordsFiltered: totalrows,
      data: rows.slice(req.body.start, req.body.start + req.body.length)
    }
    res.json(tableparams)
  })
})

/* GET ALL Services Configurations */
router.post('/all', function (req, res, next) {
  let searchJson = {}
  for (let col_ind in req.body.columns) {
    if (req.body.columns[col_ind]['search']['value'] != "") {
      searchJson[req.body.columns[col_ind]['name']] = req.body.columns[col_ind]['search']['value']
    }
  }
  AppConf.model.find(searchJson).count(function (err, count) {
    if (err) return next(err)
    let orderedColumnNum = parseInt(req.body.order[0]['column'])
    let orderedColumnStr = req.body.columns[orderedColumnNum]['name']
    console.log(searchJson)
    let dir = req.body.order[0]['dir']
    AppConf.model.find(searchJson).sort([[orderedColumnStr, dir]]).skip(Number(req.body.start)).limit(Number(req.body.length)).exec(function (err, rows) {
      if (err) return next(err)
      // Response for datatable
      var tableparams = {
        draw: req.body.draw,
        recordsTotal: count,
        recordsFiltered: count,
        data: rows
      }
      res.json(tableparams)
    })
  })
})

/* SAVE Services Configurations */
router.post('/', function (req, res, next) {
  AppConf.model.create(req.body, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* UPDATE Services Configurations */
router.put('/:id', function (req, res, next) {
  AppConf.model.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* GET last version by appl */
router.get('/lastv/:appl', function (req, res, next) {
  AppConf.model.findOne()
    .where({ appl: req.params.appl })
    .sort('-v')
    .exec(function (err, doc) {
      if (err) return next(err)
      res.json({ lastv: doc.v })
    })
})

/* FIND if App already exist */
router.get('/find/:appl', function (req, res, next) {
  AppConf.model.count({ appl: req.params.appl }, function (err, count) {
    if (err) return next(err)
    res.json({ 'count': count })
  })
})

/* DELETE Services Configurations */
router.delete('/:id', function (req, res, next) {
  AppConf.model.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* GET SINGLE Services Configurations BY ID */
router.get('/:id', function (req, res, next) {
  AppConf.model.findById(req.params.id, function (err, post) {
    if (err) return next(err)
    res.json({ row: post, infotable: AppConf.infotable })
  })
})

/* GET SINGLE Services Configurations BY ID */
router.post('/getAppIds', function (req, res, next) {
  var myjson = JSON.parse(req.body['applist'])
  for (let key in myjson) {
    console.log(key)
  }
  res.json({"pippo":"pluto"})
}) 

module.exports = router
