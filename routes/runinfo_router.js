var express = require('express')
var router = express.Router()
var RunInfo = require('../models/RunInfo.js')

/* In the runinfo table we always take all of the entries from the DB */
router.get('/', function (req, res, next) {
  res.json({infotable: RunInfo.infotable})
})

/* GET ALL Run Infos */
router.get('/all', function (req, res, next) {
  // Disabled for the runinfo table
  // RunInfo.model.find(function (err, rows) {
  //   if (err) return next(err)
  //   res.json(rows)
  // })
})

/* GET ALL Run Infos */
router.post('/getRows', function (req, res, next) {
  // Extracting possible 'order by' (only one parameter allowed)
  // First defining default as first sortable column in infotable:
  let orderBy = Object.keys(RunInfo.infotable)[0]
  let desc = 'desc'
  for (var col in RunInfo.infotable) {
    if (RunInfo.infotable[col].hasOwnProperty('sortable') && RunInfo.infotable[col].hasOwnProperty('sortable')) {
      orderBy = col
      break
    }
  }
  // Now extracting paramters from request:
  if (req.body.hasOwnProperty('order') && req.body.order.length > 0) {
    let columnNumber = req.body.order[0]['column']
    // orderBy = Object.keys(RunInfo.infotable)[columnNumber]
    orderBy = req.body.columns[columnNumber].name
    desc = req.body.order[0]['dir']
  }
  let searchMap = {}
  for (let index in req.body.columns) {
    let column = req.body.columns[index]
    if (column.search.value !== '') {
      console.log('type: ', RunInfo.infotable[column.name].type)
      console.log('value: ', column.search.value)
      if (column.name === '_id' && column.search.value.indexOf('-') > 0) {
        let runs = column.search.value.split('-')
        console.log('runs range: ', runs)
        searchMap[column.name] = { $gt: runs[0], $lt: runs[1] }
      } else if (column.name === 'startTime') {
        let date = new Date(column.search.value)
        searchMap[column.name] = { $gte: date.getTime() }
      } else if (column.name === 'stopTime') {
        let date = new Date(column.search.value)
        searchMap[column.name] = { $lte: date.getTime() }
      } else {
        searchMap[column.name] = column.search.value
      }
    }
  }
  console.log('searchMap: ', searchMap)
  // ACHTUNG!
  // There is no need to convert the query value to the desired type
  // since it is automatically made by mongoosed, based on the informations
  // contained in the schema.
  // This means that the DB has to be consistent, or the search will not work!
  // Counting all the elements in the table
  RunInfo.model.find(searchMap).count(function (err, count) {
    if (err) return next(err)
    // Creating sort string
    let sortString = orderBy
    if (desc === 'desc') sortString = '-' + sortString
    // console.log('sort: ', sortString)
    RunInfo.model.find(searchMap).sort(sortString).skip(Number(req.body.start)).limit(Number(req.body.length)).exec(function (err, rows) {
      if (err) return next(err)
      // Response for datatable
      var tableparams = {
        draw: req.body.draw,
        recordsTotal: count,
        recordsFiltered: count,
        data: rows
      }
      res.json(tableparams)
    })
  })
})

/* SAVE Run Infos */
router.post('/', function (req, res, next) {
  // disabled
})

/* UPDATE Run Infos */
router.put('/:id', function (req, res, next) {
  // disabled
})

/* DELETE Run Infos */
router.delete('/:id', function (req, res, next) {
  RunInfo.model.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* GET SINGLE Run Infos BY ID */
router.get('/:id', function (req, res, next) {
  RunInfo.model.findById(req.params.id, function (err, post) {
    if (err) return next(err)
    res.json({row: post, infotable: RunInfo.infotable})
  })
})

module.exports = router
