var express = require('express')
var router = express.Router()
var PSConfig = require('../models/PSConfig.js')
var _ = require('lodash')

router.get('/', function (req, res, next) {
  res.json({infotable: PSConfig.infotable})
})

router.post('/getRows', function (req, res, next) {
  PSConfig.model.aggregate([
    { '$sort': { 'v': -1 } },
    { '$group': {
      '_id': '$tag',
      'tag': { '$first': '$tag' },
      'v': { '$first': '$v' },
      'comment': { '$first': '$comment' },
      'updated_date': { '$first': '$updated_date' },
      'trueid': { '$first': '$_id' }
    }
    },
    { '$sort': { 'tag': 1 } }]
    , function (err, rows) {
    if (err) return next(err)
    _.forEach(rows, (val) => {
      val._id = val.trueid
      delete val.trueid
    })
    let totalrows = rows.length
    var tableparams = {
      draw: req.body.draw,
      recordsTotal: totalrows,
      recordsFiltered: totalrows,
      data: rows.slice(req.body.start, req.body.start + req.body.length)
    }
    res.json(tableparams)
  })
})

router.post('/all', function (req, res, next) {
  PSConfig.model.find().count(function (err, count) {
    if (err) return next(err)
    let orderedColumnNum = parseInt(req.body.order[0]['column'])
    let orderedColumnStr = req.body.columns[orderedColumnNum]['name']
    let dir = req.body.order[0]['dir']
    PSConfig.model.find().sort([[orderedColumnStr, dir]]).skip(Number(req.body.start)).limit(Number(req.body.length)).exec(function (err, rows) {
      if (err) return next(err)
      // Response for datatable
      var tableparams = {
        draw: req.body.draw,
        recordsTotal: count,
        recordsFiltered: count,
        data: rows
      }
      res.json(tableparams)
    })
  })
})

/* SAVE Services Configurations */
router.post('/', function (req, res, next) {
  PSConfig.model.create(req.body, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* UPDATE Services Configurations */
router.put('/:id', function (req, res, next) {
  PSConfig.model.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* GET last version by tag */
router.get('/lastv/:tag', function (req, res, next) {
  PSConfig.model.findOne()
    .where({tag: req.params.tag})
    .sort('-v')
    .exec(function (err, doc) {
      if (err) return next(err)
      res.json({lastv: doc.v})
    }
    )
})

/* FIND if TAG already exist */
router.get('/find/:tag', function (req, res, next) {
  PSConfig.model.count({ tag: req.params.tag }, function (err, count) {
    if (err) return next(err)
    res.json({'count': count})
  }
  )
})

/* DELETE Services Configurations */
router.delete('/:id', function (req, res, next) {
  PSConfig.model.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* GET SINGLE Services Configurations BY ID */
router.get('/:id', function (req, res, next) {
  PSConfig.model.findById(req.params.id, function (err, post) {
    if (err) return next(err)
    res.json({row: post, infotable: PSConfig.infotable})
  })
})

module.exports = router
