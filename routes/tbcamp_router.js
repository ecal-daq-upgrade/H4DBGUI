var express = require('express')
var router = express.Router()
var TBCamp = require('../models/TBCamp.js')
var _ = require('lodash')

router.get('/', function (req, res, next) {
  res.json({infotable: TBCamp.infotable})
})

router.post('/getRows', function (req, res, next) {
  TBCamp.model.aggregate([
    { '$sort': { 'v': -1 } },
    { '$group': {
      '_id': '$tag',
      'tag': { '$first': '$tag' },
      'v': { '$first': '$v' },
      'comment': { '$first': '$comment' },
      'open': { '$first': '$open' },
      'updated_date': { '$first': '$updated_date' },
      'trueid': { '$first': '$_id' }
    }
    },
    { '$sort': { 'tag': 1 } },
    { '$sort': { 'open': -1 }
    }], function (err, rows) {
    if (err) return next(err)
    _.forEach(rows, (val) => {
      val._id = val.trueid
      delete val.trueid
    })
    let totalrows = rows.length
    var tableparams = {
      draw: req.body.draw,
      recordsTotal: totalrows,
      recordsFiltered: totalrows,
      data: rows.slice(req.body.start, req.body.start + req.body.length)
    }
    res.json(tableparams)
  })
})

/* GET ALL Services Configurations */
router.post('/all', function (req, res, next) {
  TBCamp.model.find().count(function (err, count) {
    if (err) return next(err)
    let orderedColumnNum = parseInt(req.body.order[0]['column'])
    let orderedColumnStr = req.body.columns[orderedColumnNum]['name']
    let dir = req.body.order[0]['dir']
    TBCamp.model.find().sort([[orderedColumnStr, dir]]).skip(Number(req.body.start)).limit(Number(req.body.length)).exec(function (err, rows) {
      if (err) return next(err)
      // Response for datatable
      var tableparams = {
        draw: req.body.draw,
        recordsTotal: count,
        recordsFiltered: count,
        data: rows
      }
      res.json(tableparams)
    })
  })
})

/* SAVE Services Configurations */
router.post('/', function (req, res, next) {
  TBCamp.model.create(req.body, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* UPDATE Services Configurations */
router.put('/:id', function (req, res, next) {
  TBCamp.model.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* GET last version by tbtag */
router.get('/lastv/:tag', function (req, res, next) {
  TBCamp.model.findOne()
    .where({ tag: req.params.tag })
    .sort('-v')
    .exec(function (err, doc) {
      if (err) return next(err)
      res.json({ lastv: doc.v })
    })
})

/* FIND if TAG already exist */
router.get('/find/:tag', function (req, res, next) {
  TBCamp.model.count({ tag: req.params.tag }, function (err, count) {
    if (err) return next(err)
    res.json({'count': count})
  }
  )
})

/* DELETE Services Configurations */
router.delete('/:id', function (req, res, next) {
  TBCamp.model.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err)
    res.json(post)
  })
})

/* GET SINGLE Services Configurations BY ID */
router.get('/:id', function (req, res, next) {
  TBCamp.model.findById(req.params.id, function (err, post) {
    if (err) return next(err)
    res.json({row: post, infotable: TBCamp.infotable})
  })
})

module.exports = router
