# H4DBGUI

## Setup

### Setup the local development environment

```.bash
# install dependencies
npm install
```

### Import the test db 

Import the test db (keeping old version - for production)

```.bash
mongorestore -d test db-test/test
```

Import the test db (dump old version - for development)

```.bash
mongorestore --drop -d test db-test/test
```

### Run the development environment

Both backend and frontend in **node 6**, in compliance with centos7

```.bash
# backend - that should be manually restarted
gulp

# frontend - run with hot reload at localhost:8080
npm run dev
```

Build the frontend in order to ship it to production

```.bash
# frontend - build for production with minification
npm run build

# build for production and view the bundle analyzer report
# npm run build --report
```

## Export the db

```.bash
mongodump -d test -o db-test
```